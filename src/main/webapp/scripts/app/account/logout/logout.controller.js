'use strict';

angular.module('treeschoolApp')
    .controller('LogoutController', function (Auth) {
        Auth.logout();
    });
