'use strict';

angular.module('treeschoolApp')
    .controller('StudentDetailController', function ($scope, $rootScope, $stateParams, entity, Student) {
        $scope.student = entity;
        $scope.load = function (id) {
            Student.get({id: id}, function(result) {
                $scope.student = result;
            });
        };
        $rootScope.$on('treeschoolApp:studentUpdate', function(event, result) {
            $scope.student = result;
        });
    });
