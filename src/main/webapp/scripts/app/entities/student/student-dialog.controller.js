'use strict';

angular.module('treeschoolApp').controller('StudentDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Student',
        function($scope, $stateParams, $modalInstance, entity, Student) {

        $scope.student = entity;
        $scope.load = function(id) {
            Student.get({id : id}, function(result) {
                $scope.student = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('treeschoolApp:studentUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.student.id != null) {
                Student.update($scope.student, onSaveFinished);
            } else {
                Student.save($scope.student, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
