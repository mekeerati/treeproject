'use strict';

angular.module('treeschoolApp')
    .controller('StudentController', function ($scope, Student, ParseLinks) {
        $scope.students = [];
        $scope.page = 1;
        $scope.loadAll = function() {
            Student.query({page: $scope.page, per_page: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.students = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Student.get({id: id}, function(result) {
                $scope.student = result;
                $('#deleteStudentConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Student.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteStudentConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.student = {firstname: null, lastname: null, nickname: null, gender: null, yearOfServices: null, dob: null, comment: null, id: null};
        };
    });
