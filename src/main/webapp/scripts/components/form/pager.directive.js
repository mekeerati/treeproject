/* globals $ */
'use strict';

angular.module('treeschoolApp')
    .directive('treeschoolAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
