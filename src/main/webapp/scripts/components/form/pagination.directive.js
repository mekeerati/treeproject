/* globals $ */
'use strict';

angular.module('treeschoolApp')
    .directive('treeschoolAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
