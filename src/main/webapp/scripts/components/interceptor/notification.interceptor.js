 'use strict';

angular.module('treeschoolApp')
    .factory('notificationInterceptor', function ($q, AlertService) {
        return {
            response: function(response) {
                var alertKey = response.headers('X-treeschoolApp-alert');
                if (angular.isString(alertKey)) {
                    AlertService.success(alertKey, { param : response.headers('X-treeschoolApp-params')});
                }
                return response;
            },
        };
    });